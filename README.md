# My Personal Blog

[![Netlify Status](https://api.netlify.com/api/v1/badges/2e63a8d5-3807-462d-bbb4-4ce9f444f545/deploy-status)](https://app.netlify.com/sites/sublog/deploys)

I'm going to use this site to talk about stuff that interests me and do what I want with. Personal passion project, no major visions for the future.

Deployed with [Netlify](https://netlify.app)

## Access
You can access the website at https://sublog.netlify.app/

## Selfhost
You can host your own version of this with Netlify https://app.netlify.com/start/deploy?repository=https://gitlab.com/alanpearce/zola-bearblog or manually with the guide at https://codeberg.org/alanpearce/zola-bearblog
