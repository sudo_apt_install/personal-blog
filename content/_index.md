+++
+++
# Welcome to my blog. Sublog.

Designed to be fast, lightweight, free (not 'free'), open, private, and accessible.

This website is a fork of [Alan Pearce's bear blog theme, Zola.](https://codeberg.org/alanpearce/zola-bearblog)

View the original bear blog (without Alan Pearce's theme) [here](https://bearblog.dev/).

---

Want to host something similar yourself? The codeberg repo for this theme has info on that. https://codeberg.org/alanpearce/zola-bearblog

## Tech(ish) info:
Hoster: [Netlify](https://netlify.app)
Original repo: [alanpearce/zola-bearblog @ codeberg](https://codeberg.org/alanpearce/zola-bearblog)
Blog written in markdown.

---

## Contact
XMPP: `sudo@jix.im`
Matrix: `sudo-rm--rf:envs.net` [link](https://matrix.to/#/@sudo:envs.net)
Signal: `kali.99` [link](https://signal.me/#eu/oi-xnzLEyTZynHkwbwkvQfOnij4W_koICHeRaB_ODopMINyNMghKTazL1n67MXRn)
I'm most likely to see your message if you use XMPP.
